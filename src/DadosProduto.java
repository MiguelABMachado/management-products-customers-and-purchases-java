import java.util.*;
import java.io.*;

/**
 *
 * @author Asus
 */
public class DadosProduto implements Serializable {

    private ArrayList<Compra> minhas_compras;
    private String produto;

    /**
     * Construtor de classe com os valores por defeito
     */
    public DadosProduto() {
        this.minhas_compras = new ArrayList<>();
        this.produto = "";
    }

    
    /**
     * Construtor de clasee com os valores passados por argumento
     * @param minhas_compras  int com o valor do total de compras
     * @param produto  int com o valor do total de clientes
     */ 
    public DadosProduto(ArrayList<Compra> minhas_compras, String produto) {
       this.minhas_compras = new ArrayList<>();
        for (Compra a:minhas_compras){
            this.minhas_compras.add(a);
        }
        this.produto = produto;
    }

    /**
     * Construtor de classe com os valores de um objecto dessa mesma classe
     *
     * @param par objecto com os valores
     */
    public DadosProduto(DadosProduto par) {
        this.minhas_compras = par.getMinhas_compras();
        this.produto = par.getProduto();
    }

    /**
     * Adicona um produto
     *
     * @param s Codigo do produto
     */
    public DadosProduto(String s) {
        minhas_compras = new ArrayList<>();
        produto = s;
    }

    /**
     * Devolve uma string com o código do produto
     *
     * @return código de produto
     */
    public String getProduto() {
        return produto;
    }

    /**
     * Devolve uma lista com todas as compras
     *
     * @return lista de compras
     */
    public ArrayList<Compra> getMinhas_compras() {
        ArrayList<Compra> aux = new ArrayList<>();
        for (Compra c : this.minhas_compras) {
            aux.add(c.clone());
        }
        return aux;
    }

    /**
     * Altera o código do produto para o passado por argumento
     *
     * @param produto novo código de produto
     */
    public void setProduto(String produto) {
        this.produto = produto;
    }

    /**
     * Altera o conjunto de compras para o passado por argumento
     *
     * @param minhas_compras novo conjunto de compras
     */
    public void setMinhas_compras(ArrayList<Compra> minhas_compras) {
        ArrayList<Compra> aux = new ArrayList<>();
        for (Compra c : minhas_compras) {
            aux.add(c.clone());
        }
        this.minhas_compras = aux;
    }

    /**
     * Adiciona uma compra a um cliente
     *
     * @param c compra a adicionar
     */
    public void add_compra(Compra c) {
        minhas_compras.add(c.clone());
    }

    /**
     * Devolve a quantidade de produtos comprados
     * @return a quantidade de produtos comprados
     */
    public int get_tot_sold() {
        int tot = 0;
        for (Compra c : minhas_compras) {
            tot += c.getQuantidade();
        }
        return tot;
    }
    
    /**
     * Verifica se dois objectos são iguais
     * @param o objecto a qual deve ser comparado
     * @return true se foram iguais, false caso contrário
     */
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if ((o == null) || (this.getClass() != o.getClass())) {
            return false;
        }
        return false;
    }

    /**
     * Cria um clone deste objecto
     * @return clone do objecto
     */
    public DadosProduto clone() {
        return new DadosProduto(this);
    }
    
    /**
     * Retorna os dados do objecto em formato de texto
     * @return string com a informação do objecto
     */
    public String toString(){
    StringBuilder s = new StringBuilder();
    s.append(this.getProduto());
    s.append(System.lineSeparator());
    for(Compra c:minhas_compras){
        s.append(c.toString());
        s.append(System.lineSeparator());
    }
    return s.toString();
    }

}
