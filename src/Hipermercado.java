import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Asus
 */
public class Hipermercado {
    
    
    private static Dados info=new Dados();
    private static Menu menu_inicial,menu_principal;
    
    public static void main(String [] args){
        carregaMenus();
        exec_menu_inicial();
    }
    
 private static void nova_leitura(){
    limpaEcra();
    System.out.println("Insira o nome do ficheiro de compras");
    String filename=Input.lerString();
    Crono.start();   
    info=new Dados();
    info.lerClientes2("FichClientes.txt");
    info.lerProdutos2("FichProdutos.txt");
    info.lerCompras2(filename);
    Crono.stop();
    System.out.println("Demorou "+Crono.print()+" segundos");
    mostraMensagem();
    exec_menu_principal();
 }
 
 private static void guardaDados(){
     limpaEcra();
     System.out.println("Insira o nome com que deseja guardar");
     String filename=Input.lerString();
     Crono.start(); 
     if(filename.equals("")){
         try {
             info.gravaObj("hipermercado.obj");
         } catch (IOException ex) {
             Logger.getLogger(Hipermercado.class.getName()).log(Level.SEVERE, null, ex);
         }
     }else{
         try {
             info.gravaObj(filename);
         } catch (IOException ex) {
             Logger.getLogger(Hipermercado.class.getName()).log(Level.SEVERE, null, ex);
         }
     }
     Crono.stop();
     System.out.println("Demorou "+Crono.print()+" segundos");
     mostraMensagem();
     exec_menu_principal();
         
 }   
  
  private static void carregaDados() throws IOException,ClassNotFoundException{
        limpaEcra();
        info = new Dados();
        System.out.println("Escreva o nome do ficheiro.");
        String filename=Input.lerString();
        Crono.start();
        FileInputStream door = new FileInputStream(filename); 
        ObjectInputStream reader = new ObjectInputStream(door); 
        if(info instanceof Dados)
            info = (Dados) reader.readObject();
        Crono.stop();
        System.out.println("Demorou "+Crono.print()+" segundos");
        mostraMensagem();
        exec_menu_principal();
    }
    
  private static void carregaMenus(){
        String[] inicial={"Ficheiro de texto",
                          "Ficheiro objecto",
                          }; 
        
        String[] principal={"Lista dos códigos de produtos nunca comprados",
                            "Lista dos códigos de clientes que nunca realizaram compras",
                            "Total de compras e clientes de um determinado mês",
                            "Total de compras e produtos mês a mês de um determinado cliente",
                            "Total de compras mês a mês de um determinado produto",
                            "Total de compras mês a mês de um determinado produto distinguido entre N e P",
                            "Top produtos comprados por um cliente",
                            "Top X produtos mais vendidos durante o ano",
                            "Top X clientes que compraram maior número de produtos distintos",
                            "Top X de clientes que compraram um determinado produto",
                            "Ler novo ficheiro de compras",
                            "Carregar novos dados",
                            "Guardar dados",
                            "Ver estatisticas"
                          }; 
        
        menu_principal=new Menu(principal);
        menu_inicial=new Menu(inicial);
    }

    private static void exec_menu_inicial() {
        do {
            limpaEcra();
            System.out.println("************ Bem-vindo a Gesthiper ************");
            menu_inicial.executa();
            switch (menu_inicial.getOpcao()) {
                case 1:from_text();
                       break;
                case 2:{
                try {
                    carregaDados();
                } catch (IOException ex) {
                    Logger.getLogger(Hipermercado.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(Hipermercado.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
                        
            }
        }while (menu_inicial.getOpcao()!=0);
        sair();
    }
    
    private static void exec_menu_principal() {
        do {
            limpaEcra();
            System.out.println("************ Bem-vindo a Gesthiper ************");
            menu_principal.executa();
            switch (menu_principal.getOpcao()) {
                case 1: querie1();
                    break;
                case 2: querie2();
                    break;
                case 3: querie3();    
                    break;
                case 4:querie4();
                    break;
                case 5:querie5();
                    break;
                case 6:querie6();
                    break;
                case 7:querie7();
                    break;
                case 8:querie8();
                    break;
                case 9:querie9();
                    break;
                case 10:querie10();
                    break;
                case 11:nova_leitura();
                    break;
                case 12:{
                    try {
                        carregaDados();
                    } catch (IOException ex) {
                        Logger.getLogger(Hipermercado.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (ClassNotFoundException ex) {
                        Logger.getLogger(Hipermercado.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                    break;
                case 13:guardaDados();
                    break;
                case 14:ver_stats(); 
                    
                }
        }while (menu_principal.getOpcao()!=0);
        sair();
    }
    
    private static void sair(){
       System.exit(0);
    }
    
    private final static void limpaEcra (){
       System.out.print('\u000C');
    }

    private static void from_text() {
        limpaEcra();
        Crono.start();
        info.lerClientes2("FichClientes.txt");
        info.lerProdutos2("FichProdutos.txt");
        info.lerCompras2("Compras.txt");
        Crono.stop();
        System.out.println("Demorou "+Crono.print()+" segundos");
        mostraMensagem();
        exec_menu_principal();
    }

    private static void ver_stats() {
        limpaEcra();
        boolean opcao_val=false;
        int opcao;
        int[] aux1=info.get_tot_compras_mensal();
        int[] aux2=info.get_tot_compras_mensal();
        float[] aux3=info.get_tot_faturacao_mensal();
        System.out.println(info.stats_em_texto());
        mostraMensagem();
        limpaEcra();
        System.out.println("Total de compras mês a mês");
        print_array(aux1);
        mostraMensagem();
        limpaEcra();
        System.out.println("Total de clientes mês a mês");
        print_array(aux2);
        mostraMensagem();
        limpaEcra();
        System.out.println("Total de faturação mês a mês");
        print_array(aux3);
        mostraMensagem();
        limpaEcra();
        ArrayList<Compra> invalidas= info.get_invalidas();
        print_array_list(invalidas);
        System.out.println("Deseja guardar a lista de compras inválidas num ficheiro?");
        while(!opcao_val){
            System.out.println("Pressione 1 para guardar");
            System.out.println("Pressione 2 para não guardar");
            opcao=Input.lerInt();
            if(opcao==1){
                array_list_para_ficheiro(invalidas);
                opcao_val=true;
            }else{
                if(opcao==2){opcao_val=true;
                }else{
                    System.out.println("Opção inválida");
                }
            }
        }
        exec_menu_principal();
    }
    
    private static void mostraMensagem(){
        
        Scanner reader = new Scanner(System.in);
        String c;
        System.out.println("--- Prima <e> para sair ---");
        do {
            c = Input.lerString();
        }
        while(c.endsWith("e") != true);
        
    }

    
    private static void querie1(){
        limpaEcra();
        Crono.start();
        ParArrayListInt res=new ParArrayListInt();
        res=info.get_by_order_prod_not_comp();
        ArrayList<String> aux=res.getCodigo();
        Crono.stop();
        System.out.println("Demorou "+Crono.print()+" segundos");
        if(aux.size()==0){
            System.out.println("Todos os produtos foram comprados");
            mostraMensagem();
        }else{
            print_array_list_string(aux);
        }
        exec_menu_principal();
    }
    
    private static void querie2(){
        limpaEcra();
        Crono.start();
        ParArrayListInt res=new ParArrayListInt();
        res=info.get_by_order_client_not_comp();
        ArrayList<String> aux=res.getCodigo();
        Crono.stop();
        System.out.println("Demorou "+Crono.print()+" segundos");
        if(aux.size()==0){
            System.out.println("Todos os clientes realizaram compras");
            mostraMensagem();
        }else{
            print_array_list_string(aux);
        }
        exec_menu_principal();
    }
    
    private static void querie3(){
        limpaEcra();
        System.out.println("Insira um mês");
        int mes = Input.lerInt();
        Crono.start();
        ParTotComprasTotClientes par=info.get_tot_cliente_compras_mes(mes,12);
        Crono.stop();
        System.out.println("Demorou "+Crono.print()+" segundos");
        System.out.println("Total de compras no mes "+mes+"->"+par.getTotCompras());
        System.out.println("Total de clientes que compraram no mes "+mes+"->"+par.getTotClientes());
        mostraMensagem();
        exec_menu_principal();
    }
       
    private static void querie4(){
        limpaEcra();
        int i=0;
        System.out.println("Insira um código de cliente.Ex:AA111");
        String cliente=Input.lerString();
        Crono.start();
        Dois_array_int_array_float_float aux = new Dois_array_int_array_float_float();
        
        int[] aux2;
        float[] aux3;
        try {
            aux= info.get_dados_mensal_cliente(cliente);
            Crono.stop();
            System.out.println("Demorou "+Crono.print()+" segundos");
            System.out.println("Compras mês a mês:");
            System.out.println();
            aux2=aux.getCompras_mes_a_mes();
            print_array(aux2);
            mostraMensagem();
            System.out.println("Produtos mês a mês:");
            System.out.println();
            aux2=aux.getTot_prod_mes_a_mes();
            print_array(aux2);
            mostraMensagem();
            System.out.println("Faturação mês a mês:");
            System.out.println();
            aux3=aux.getFaturacao_mes_a_mes();
            print_array(aux3);
            System.out.println("No total gastou: "+aux.getTot_faturacao());
            mostraMensagem();
            exec_menu_principal();
        } catch (CodigoExisteException ex) {
            System.out.println("Cliente inexistente");
        }
        exec_menu_principal();
    }

    private static void querie5(){
        limpaEcra();
        System.out.println("Insira um produto");
        String produto = Input.lerString();
        Crono.start();
        TriTotComprasTotClientesTotFaturado tri = info.get_tot_comp_cliente_mes_a_mes(produto,12);
        Crono.stop();
        System.out.println("Demorou "+Crono.print()+" segundos");
        System.out.println("O produto "+produto+" foi comprado:");
        print_array(tri.getTotCompras());
        mostraMensagem();
        System.out.println("O produto "+produto+" foi comprado por tantos clientes");
        print_array(tri.getTotClientes());
        mostraMensagem();
        System.out.println("O produto "+produto+" teve esta faturação");
        print_array(tri.getTotFaturado());
        mostraMensagem();
        exec_menu_principal();
    }
    
    private static void querie6(){
        limpaEcra();
        System.out.println("Insira um código de produto.");
        String produto =Input.lerString();
        Crono.start();
        DoisArraysInt_DoisArraysFloats res=info.prod_comp_fat_mes_a_mes(produto);
        Crono.stop();
        System.out.println("Demorou "+Crono.print()+" segundos");
        int[] aux=res.get_A();
        float[] aux2=res.get_C();
        System.out.println("Total de compras do tipo N do produto "+produto+" mês a mês");
        System.out.println();
        print_array(aux);
        mostraMensagem();
        limpaEcra();
        System.out.println("Total de faturação do tipo N do produto "+produto+" mês a mês");
        System.out.println();
        print_array(aux2);
        mostraMensagem();
        limpaEcra();
        aux=res.get_B();
        aux2=res.get_D();
        System.out.println("Total de compras do tipo P do produto "+produto+" mês a mês");
        System.out.println();
        print_array(aux);
        mostraMensagem();
        limpaEcra();
        System.out.println("Total de faturação do tipo P do produto "+produto+" mês a mês");
        System.out.println();
        print_array(aux2);
        mostraMensagem();
        exec_menu_principal();
    }
    
    private static void querie7(){
        limpaEcra();
        System.out.println("Insira um código de cliente");
        String cliente=Input.lerString();
        Crono.start();
        ArrayList<Codigo_e_total> res =info.get_all_comp_ord(cliente);
        Crono.stop();
        System.out.println("Demorou "+Crono.print()+" segundos");
        print_array_list_7(res);
        mostraMensagem();
        exec_menu_principal();
    }
    
    private static void querie8(){
        limpaEcra();
        int i=0;
        System.out.println("Insira o número de resultados que deseja");
        int x=Input.lerInt();
        Crono.start();
        TreeSet<Codigo_e_total> res =info.get_most_sold();
        Crono.stop();
        System.out.println("Demorou "+Crono.print()+" segundos");
        Iterator<Codigo_e_total> iterator = res.iterator();
        while (iterator.hasNext() && i<x) {
            Codigo_e_total ct=iterator.next();
            if(ct.getTotal()!=0){
        System.out.println(ct.toString()+"     "+info.tot_client_comp(ct.getCodigo()));
            }
                i++;
    }
        mostraMensagem();
        exec_menu_principal();
    }
    
    private static void querie9(){
        limpaEcra();
        int i=0;
        System.out.println("Insira o número de resultados que deseja");
        int x=Input.lerInt();
        Crono.start();
        TreeSet<Codigo_e_total> res =info.get_most_diverse_ord();
        Crono.stop();
        System.out.println("Demorou "+Crono.print()+" segundos");
        Iterator<Codigo_e_total> iterator = res.iterator();
        while (iterator.hasNext() && i<x) {
            Codigo_e_total ct=iterator.next();
            if(ct.getTotal()!=0){
        System.out.println(ct.toString());
            }
                i++;
    }
        mostraMensagem();
        exec_menu_principal();
    }
    
    private static void querie10(){
        limpaEcra();
        int i=0;
        System.out.println("Insira um código de produto");
        String produto=Input.lerString();
        System.out.println("Insira o número de resultados que deseja");
        int x=Input.lerInt();
        Crono.start();
        TreeSet<Codigo_e_total> res =info.get_top_buyers_ord(produto);
        Crono.stop();
        System.out.println("Demorou "+Crono.print()+" segundos");
        Iterator<Codigo_e_total> iterator = res.iterator();
        while (iterator.hasNext() && i<x) {
            Codigo_e_total ct=iterator.next();
            if(ct.getTotal()!=0){
        System.out.println(ct.toString());
            }
                i++;
    }
        mostraMensagem();
        exec_menu_principal();
        
    }
    
    private static void array_list_para_ficheiro(ArrayList<Compra> array) {
        String filename;
        System.out.println("Insira o nome do ficheiro em que deseja guardar");
        filename=Input.lerString();
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(filename, "UTF-8");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Hipermercado.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Hipermercado.class.getName()).log(Level.SEVERE, null, ex);
        }
        for(Compra c:array){
            writer.println(c.toString());
        }
        System.out.println("Guardado com sucesso");
        mostraMensagem();
        writer.close();
    }
    
    private static void print_array(int[] array){
        int i;
        System.out.println("Mês    |Total");
        System.out.println();
        for(i=0;i<array.length;i++){
            System.out.print(i+1);
            if(i<9){
                System.out.print("      |");
            }else{
                System.out.print("     |");
            }
            System.out.println(array[i]);
        }
    }
    
    private static void print_array(float[] array){
        int i;
        System.out.println("Mês    |Total");
        System.out.println();
        for(i=0;i<array.length;i++){
            System.out.print(i+1);
            if(i<9){
                System.out.print("      |");
            }else{
                System.out.print("     |");
            }
            System.out.println(array[i]);
        }
    }
    
    private static void print_array_list(ArrayList<Compra> array) {
        int size=array.size(),pag=0,i=0,pag_atual=1,aux=0,opcao;
        boolean parar=false,opcao_val;
        System.out.println("Total de resultados: "+size);
        if(size%15!=0){
        pag=(size/15)+1;
    }else{
        pag=size/15;
    }
        System.out.println("Total de páginas: "+pag);
        while(i<size && !parar){
            if(aux==0){
                System.out.println("A mostrar página "+pag_atual+"/"+pag);
            }
            Compra c=array.get(i);
            System.out.println(c.toString());
            i++;
            aux++;
            if(aux==15){
                opcao_val = false;
                System.out.println();
                System.out.println("Próximos 15 pressione 1");
                System.out.println("Anteriores 15 pressione 2");
                System.out.println("Sair pressione 3");
                opcao=Input.lerInt();
                aux=0;
                while(!opcao_val){
                    switch(opcao){
            case 3:{
                            parar=true;
                            opcao_val=true;
                            }break;

                        case 1:{
                            opcao_val=true;
                            pag_atual++;
                            } break;

            case 2:{
                            i=i-30;
                            pag_atual--;
                            if(pag_atual<1){
                                pag_atual=1;
                            }
                            if(i<0){
                i=0;
                            }
                            opcao_val=true;
                            };break;
                            
                        default:{System.out.println("Insira uma opção válida");opcao=Input.lerInt();}
                    }
                }
                limpaEcra();
            }
        }
    }
    
    private static void print_array_list_string(ArrayList<String> array) {
        int size=array.size(),pag=0,i=0,pag_atual=1,aux=0,opcao;
        boolean parar=false,opcao_val;
        System.out.println("Total de resultados: "+size);
        if(size%15!=0){
        pag=(size/15)+1;
    }else{
        pag=size/15;
    }
        System.out.println("Total de páginas: "+pag);
        while(i<size && !parar){
            if(aux==0){
                System.out.println("A mostrar página "+pag_atual+"/"+pag);
            }
            System.out.println(array.get(i));
            i++;
            aux++;
            if(aux==15){
                opcao_val = false;
                System.out.println();
                System.out.println("Próximos 15 pressione 1");
                System.out.println("Anteriores 15 pressione 2");
                System.out.println("Sair pressione 3");
                opcao=Input.lerInt();
                aux=0;
                while(!opcao_val){
                    switch(opcao){
            case 3:{
                            parar=true;
                            opcao_val=true;
                            }break;

                        case 1:{
                            opcao_val=true;
                            pag_atual++;
                            } break;

            case 2:{
                            i=i-30;
                            pag_atual--;
                            if(pag_atual<1){
                                pag_atual=1;
                            }
                            if(i<0){
                i=0;
                            }
                            opcao_val=true;
                            };break;
                            
                        default:{System.out.println("Insira uma opção válida");opcao=Input.lerInt();}
                    }
                }
                limpaEcra();
            }
        }
    }
    
    private static void print_array_list_7(ArrayList<Codigo_e_total> array) {
        int size=array.size(),pag=0,i=0,pag_atual=1,aux=0,opcao;
        boolean parar=false,opcao_val;
        System.out.println("Total de resultados: "+size);
        if(size%15!=0){
        pag=(size/15)+1;
    }else{
        pag=size/15;
    }
        System.out.println("Total de páginas: "+pag);
        while(i<size && !parar){
            if(aux==0){
                System.out.println("A mostrar página "+pag_atual+"/"+pag);
            }
            Codigo_e_total c=array.get(i);
            System.out.println(c.toString());
            i++;
            aux++;
            if(aux==15){
                opcao_val = false;
                System.out.println();
                System.out.println("Próximos 15 pressione 1");
                System.out.println("Anteriores 15 pressione 2");
                System.out.println("Sair pressione 3");
                opcao=Input.lerInt();
                aux=0;
                while(!opcao_val){
                    switch(opcao){
            case 3:{
                            parar=true;
                            opcao_val=true;
                            }break;

                        case 1:{
                            opcao_val=true;
                            pag_atual++;
                            } break;

            case 2:{
                            i=i-30;
                            pag_atual--;
                            if(pag_atual<1){
                                pag_atual=1;
                            }
                            if(i<0){
                i=0;
                            }
                            opcao_val=true;
                            };break;
                            
                        default:{System.out.println("Insira uma opção válida");opcao=Input.lerInt();}
                    }
                }
                limpaEcra();
            }
        }
    }
}


  
    

