import java.util.*;
import java.io.*;

/**
 *
 * @author Asus
 */
public class Produto implements Serializable {
    
private String id;

   // Construtores
    

    /**
     * Construtor de classe com os valores por defeito
     */
    public Produto() {
        id="";
    }

    /**
     * Construtor de clasee com os valores passados por argumento
     * @param id valor do id
     */
    public Produto(String id) {
        this.id = id;
    }
    
    /**
     * Construtor de classe com os valores de um objecto dessa mesma classe 
     * @param c objecto com os valores
     */
    public Produto(Produto c){
        this.id= c.getId();
    }
    
    //Get's e Set's

    /**
     * Devolve o id de um Cliente
     * @return string com o valor de id
     */
    public String getId() {
        return id;
    }

    /**
     * Altera o valor de id de Cliente para o valor passado por argumento
     * @param id string com o valor de id
     */
    public void setId(String id) {
        this.id = id;
    }

     /**
     * Retorn uma cópia de um produto
     * @return Produto com a cópia
     */
    public Produto clone(){
       return new Produto(this);
   }

    /**
     * Verifica se um Produto é igual a outro Produto
     * @param o Produto a qual deve ser comparado
     * @return true se os Produtoos foram iguais,false caso contrário
     */
    public boolean equals(Object o) {
       if(o==this) return true;
        if((o==null)||(this.getClass()!=o.getClass())){
            return false;}
        Produto a=(Produto) o;
        return (this.id.equals(a.getId()));
    }

   /**
     * Devolve a informação do produto sobre a forma de texto
     * @return string com a informação de produto
     */
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append("Código de protudo: ");
        s.append(getId());
        s.append(System.lineSeparator());
        return s.toString();
    }
    
     
    
}


