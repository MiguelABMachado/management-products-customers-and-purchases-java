import java.util.*;
import java.io.*;


/**
 *
 * @author Miguel
 */
public class ParArrayListInt implements Serializable {
    
    private ArrayList<String> codigo; 
    private int total;
    
    
     /**
     * Construtor de classe com os valores por defeito
     */
    public ParArrayListInt(){
        this.codigo = new ArrayList<>();
	this.total = 0;
    }
    
    
    /**
     * Construtor de classe com os valores passados por argumento
     * @param codigo  lista com os códigos
     * @param total  int com o valor do total
     */
    public ParArrayListInt (ArrayList<String> codigo, int total) {
        this.codigo = new ArrayList<>();
        for (String a:codigo){
            this.codigo.add(a);
        }
	this.total = total;
    }
    
   
    
    /**
     * Construtor de classe com os valores de um objecto dessa mesma classe 
     * @param k objecto com os valores
     */
    public ParArrayListInt (ParArrayListInt k){
	this.codigo = k.getCodigo();
	this.total = k.getTotal();
    }
    
    
     /**
     * Devolve uma lista de strings com o codigos 
     * @return lista de codigos
     */
    public ArrayList<String> getCodigo() {
        ArrayList<String> aux = new ArrayList<>();
	for (String a:this.codigo){
            aux.add(a);
        }
        return aux;
    }
    
    /**
     * Altera lista de Strings passado como argumento
     * @param codigo nova lista de codigos
     */
    public void setCodigo(ArrayList<String> codigo) {
       for (String a:codigo){
            this.codigo.add(a);
        }
    }
    
    
    /**
     * Devolve um inteiro com o valor do total
     * @return valor do total
     */
    public int getTotal() {
        return total;
    }
    
    
    /**
     * Altera o valor do total para o passado por argumento
     * @param total novo valor do total
     */
    public void setTotal(int total) {
	this.total = total;
    }
    
    
    /**
     * Cria um clone de um objecto desta classe
     * @return o objecto clonado
     */
    public ParArrayListInt clone(){
        return new ParArrayListInt(this);
    }

    
     /**
     * Devolve a informação do objecto sobre a forma de texto
     * @return string com a informação
     */
    public String toString(){
        StringBuilder s = new StringBuilder();
        s.append(this.getTotal());
        s.append("      "); 
        for(String a:codigo){
        s.append(a.toString());
        s.append(System.lineSeparator());
        }
        return s.toString();
    }
    
    /**
     * Verifica se dois objectos são iguais
     * @param o objecto a qual deve ser comparado
     * @return true se foram iguais, false caso contrário
     */
    public boolean equals(Object o) {
       if(o==this) return true;
        if((o==null)||(this.getClass()!=o.getClass())){
            return false;}
        ParArrayListInt a=(ParArrayListInt) o;
        
        return (this.total == a.getTotal()&& codigo.containsAll(a.getCodigo()) && a.getCodigo().containsAll(codigo));
    }

    
    
    
    
}
