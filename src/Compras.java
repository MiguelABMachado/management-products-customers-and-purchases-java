import java.util.*;
import java.io.*;

/**
 *
 * @author Asus
 */
public class Compras implements Serializable {
    
    
    private HashMap<String,DadosCliente> database_cliente;
    
    /**
     * Construtor de classe com os valores por defeito
     */
    public Compras(){
        database_cliente= new HashMap<String,DadosCliente>();
    }
    
    /**
     * Construtor de classe com os valores de objecto dessa mesma classe 
     * @param c objecto com valores
     */
    public Compras(Compras c){
        database_cliente=c.getDatabase_cliente();
    }

    /**
     * Devolve a base de dados de clientes
     * @return base de dados de clientes
     */
    public HashMap<String, DadosCliente> getDatabase_cliente() {
        HashMap<String, DadosCliente> aux=new HashMap<>();
        for(DadosCliente c:database_cliente.values()){
            aux.put(c.getCliente(), c);
        }
        return aux;
    }

    /**
     * Altera o valor da base de dados de clientes para o enviado de argumento
     * @param database_cliente novos valores da base de clientes
     */
    public void setDatabase_cliente(HashMap<String, DadosCliente> database_cliente) {
        HashMap<String, DadosCliente> aux=new HashMap<>();
        for(DadosCliente c:database_cliente.values()){
            aux.put(c.getCliente(), c);
        }
        this.database_cliente = aux;
    }
    
    

    /**
     * Adiciona um cliente sem compras
     * @param line códig de cliente
     */
    public void add_compra(String line) {
        DadosCliente aux= new DadosCliente(line);
        database_cliente.put(line, aux);
    }
    
    /**
     * Adiciona uma compra a um cliente
     * @param c compra a adicionar
     */
    public void add_compra(Compra c){
        String cliente=c.getCliente();
        if(database_cliente.containsKey(cliente)){
            DadosCliente aux= database_cliente.get(cliente);
            aux.add_compra(c.clone());
            database_cliente.put(cliente,aux);
        }
    }

    /**
     * Devolve o total de clientes que não realizaram nenhuma compra
     * @return total de clientes sem compras
     */
    public int get_tot_not_comp() {
        int tot=0;
        for(DadosCliente d: database_cliente.values()){
            ArrayList<Compra> c=d.getMinhas_compras();
            if(c.size()==0){
                tot++;
            }
        }
        return tot;
    }

    /**
     * Total de clientes que realizaram compras em cada mês
     * @param i número de meses
     * @return array com o número de clientes por mês
     */
    public int[] tot_cliente_por_mes(int i) {
        int[] aux=new int[i];
        init_array(aux,i);
        int[] ja_comprei=new int[i];
        for(DadosCliente d:database_cliente.values()){
            init_array(ja_comprei,i);
            ArrayList<Compra> a=d.getMinhas_compras();
            for(Compra c:a){
                int mes=c.getMes();
                if(ja_comprei[mes-1]==0){
                    aux[mes-1]++;
                    ja_comprei[mes-1]=1;
                }
                if(all_one(ja_comprei)){
                    break;
                }
            }
        }
        return aux.clone();
    }
    
    
    private void init_array(int[] array, int size){
        int i;
        for(i=0;i<size;i++){
            array[i]=0;
        }
    }
    
    private void init_array(float[] array, int size){
        int i;
        for(i=0;i<size;i++){
            array[i]=0;
        }
    }

    private boolean all_one(int[] array) {
        int i;
        for(i=0;i<array.length;i++){
            if(array[i]==0){
                return false;
            }
        }
        return true;
    }
    
    /**
     * Devolve um resumo da informação do cliente, o total de compras mês a mês,o total de produtos distintos mês a mês,o total de faturação mês a mês e o total de faturação mensal
     * @param cliente código de cliente
     * @return resumo da informação de cliente
     * @throws CodigoExisteException não existe cliente com esse código
     */
    public Dois_array_int_array_float_float get_dados_mensal_cliente(String cliente) throws CodigoExisteException {
        int i;
        Dois_array_int_array_float_float aux = new Dois_array_int_array_float_float();
        int[] compras_mes_a_mes;
        int[] tot_prod_mes_a_mes;
        float[] faturacao_mes_a_mes;
        float tot_faturacao;
        if(database_cliente.containsKey(cliente)){
            DadosCliente d=database_cliente.get(cliente);
            compras_mes_a_mes=d.get_compras_mes_a_mes();
            tot_prod_mes_a_mes=d.get_prod_mes_a_mes();
            faturacao_mes_a_mes=d.get_fat_mes_a_mes();
            tot_faturacao=0;
            for(i=0;i<faturacao_mes_a_mes.length;i++){
                tot_faturacao+=faturacao_mes_a_mes[i];
            }
            aux=new Dois_array_int_array_float_float(compras_mes_a_mes, tot_prod_mes_a_mes, faturacao_mes_a_mes, tot_faturacao);
        }else{
            throw new CodigoExisteException();
        }
        return aux;
    }
    
    /**
     * Devolve uma lista com os códigos de clientes que não realizaram compras ordenada alfabeticamente e o seu total
     * @return lista dos códigos de cliente e total
     */
    public ParArrayListInt get_not_comp_by_order() {
        int total=0;
        ArrayList<String> aux = new ArrayList<>();
        for (DadosCliente a:database_cliente.values()) {
            ArrayList<Compra> k = a.getMinhas_compras();
            if (k.isEmpty()){
                aux.add(a.getCliente());
                total++;
            }
        }
        Collections.sort(aux);
        return new ParArrayListInt(aux,total);
    }
    
    /**
     * Devolve o total de clientes e o total de compras de um mês enviado por argumento
     * @param mes mês de qual devemos devolver os dados
     * @param i total de meses
     * @return total de clientes e total de compras
     */
    public ParTotComprasTotClientes get_tot_compras_clientes_num_mes(int mes,int i){
        int totCompras=0;
        int totClientes=0;
        int[] aux = new int[i];
        for(DadosCliente d:database_cliente.values()){
            ArrayList<Compra> a = d.getMinhas_compras();
            for(Compra c:a){
                if(c.getMes()==mes){
                    totCompras++;
                }
            }
        }
        aux=this.tot_cliente_por_mes(i);
        totClientes=aux[mes-1];
        ParTotComprasTotClientes par = new ParTotComprasTotClientes(totCompras,totClientes);
        return par;
    }
    
    /**
     * Devolve total de clientes mês a mês que compraram um produto enviado por argumento
     * @param produto código de produto a verificar se o cliente comprou
     * @param i número de meses
     * @return total de clientes mês a mês
     */
    public int[] get_tot_cliente_mes_a_mes(String produto,int i) {
        int[] totClientes=new int[i];
        init_array(totClientes,i);
        int[] ja_comprei=new int[i];
        for(DadosCliente d:database_cliente.values()){
            init_array(ja_comprei,i);
            ArrayList<Compra> a=d.getMinhas_compras();
            for(Compra c:a){
                if(c.getProduto().equals(produto)){
                    int mes=c.getMes();
                    if(ja_comprei[mes-1]==0){
                        totClientes[mes-1]++;
                        ja_comprei[mes-1]=1;
                    }
                    if(all_one(ja_comprei)){
                        break;
                    }
                }
            }
        }
        return totClientes.clone();
    }
    
    /**
     * Devolve todos os produtos que um cliente comprou por ordem decrescente de quantidade
     * @param cliente código do cliente
     * @return todos os produtos por ordem decrescente
     */
    public TreeSet<Codigo_e_total> get_all_comp_ord(String cliente){
        TreeSet<Codigo_e_total> ts = new TreeSet<>();
        if(database_cliente.containsKey(cliente)){
            ts=database_cliente.get(cliente).get_all_comp_ord();
        }
        return ts;
    }
    
    /**
     * Devolve todos os clientes ordenados por ordem decrescente de total de quantidade comprada do produto com o código enviado por argumento
     * @param produto código de produo
     * @return todos os cliente por ordem decrescente de quantidade comprada do produto
     */
    public TreeSet<Codigo_e_total> get_top_buyers_ord(String produto){
        TreeSet<Codigo_e_total> ts = new TreeSet<>(new Codigo_e_total_comparator());
        for(DadosCliente d:database_cliente.values()){
            Codigo_e_total ct=d.tot_fat_of_prod(produto);
            ts.add(ct);
        }
        return ts;
    }
    
    /**
     * Devolve os clientes que compraram maior variadade de produtos por ordem decrescente e o total de produtos que compraram
     * @return clientes que compraram maior variadade de produtos e total
     */
    public TreeSet<Codigo_e_total> get_most_diverse_ord(){
        TreeSet<Codigo_e_total> res = new TreeSet<>(new Codigo_e_total_comparator());
        for (DadosCliente a :database_cliente.values()){
            res.add(new Codigo_e_total(a.getCliente(), a.tot_dif_comprados()));
        }  
        return res;
    }

    /**
     * Devolve o número de clientes que compraram um produto
     * @param codigo código do produto
     * @return número de clientes que compraram o produto
     */
    public int get_tot_cliente_comp(String codigo) {
        int tot=0;
        for(DadosCliente c: database_cliente.values()){
            if(c.comprei(codigo)){
                tot++;
            }
        }
        return tot;
    }
    
    /**
     * Devolve o total de clientes distintos por mês
     * @param i número de meses
     * @return total de clientes distintos por mês
     */
    public int[] tot_dist_cliente_por_mes(int i) {
        int size=0;
        int[] res=new int[i];
        init_array(res,i);
        HashMap<Integer,HashSet<String>> aux2=new HashMap<>();
        init_hash_map(aux2,i);
        for(DadosCliente d:database_cliente.values()){
            ArrayList<Compra> aux=d.getMinhas_compras();
            for(Compra c:aux){
                int mes=c.getMes();
                String cliente=c.getCliente();
                HashSet<String> aux3=aux2.get(mes-1);
                aux3.add(cliente);
                aux2.put(mes-1, aux3);
            } 
        }
        for(HashSet<String> s:aux2.values()){
            res[size]=s.size();
            size++;
        }
        return res;
    }
    
    private void init_hash_map(HashMap<Integer, HashSet<String>> aux2, int size) {
        int i;
        for(i=0;i<size;i++){
           HashSet<String> aux=new HashSet<>();
           aux2.put(i,aux);
       }
    }
    
    /**
     * Devolve a informação do objecto sobre a forma de texto
     * @return string com informação
     */
    public String toString(){
        StringBuilder s=new StringBuilder();
        for(DadosCliente c:database_cliente.values()){
            s.append(c.toString());
            s.append(System.lineSeparator());
        }
        return s.toString();
    }
    
    /**
     * Cria um clone de um objecto desta classe
     * @return o objecto clonado
     */
    public Compras clone(){
        return new Compras(this);
    }
    
    /**
     * Verifica se dois objectos são iguais
     * @param o objecto a qual deve ser comparado
     * @return true se foram iguais, false caso contrário
     */
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if ((o == null) || (this.getClass() != o.getClass())) {
            return false;
        }
        return false;
    }
}
