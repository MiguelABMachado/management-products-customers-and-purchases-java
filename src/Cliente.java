import java.util.*;
import java.io.*;

/**
 *
 * @author Asus
 */

public class Cliente implements Serializable {
    
    private String id;

   
    /**
     * Construtor de classe com os valores por defeito
     */
    public Cliente() {
        id="";
    }
    
    /**
     * Construtor de clasee com os valores passados por argumento
     * @param id valor do id
     */
    public Cliente(String id) {
        this.id = id;
    }
    
    /**
     * Construtor de classe com os valores de um objecto dessa mesma classe 
     * @param c objecto com os valores
     */
    public Cliente(Cliente c){
        this.id= c.getId();
    }
    
    /**
     * Devolve o id de um Cliente
     * @return string com o valor de id
     */
    public String getId() {
        return id;
    }

    /**
     * Altera o valor de id de Cliente para o valor passado por argumento
     * @param id string com o valor de id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Retorn uma cópia de um cliente
     * @return Cliente com a cópia
     */
    public Cliente clone(){
       return new Cliente(this);
   }

    /**
     * Verifica se um Cliente é igual a outro Cliente
     * @param o Cliente a qual deve ser comparado
     * @return true se os Clientes foram iguais,false caso contrário
     */
    public boolean equals(Object o) {
       if(o==this) return true;
        if((o==null)||(this.getClass()!=o.getClass())){
            return false;}
        Cliente a=(Cliente) o;
        return (this.id.equals(a.getId()));
    }

    /**
     * Devolve a informação do cliente sobre a forma de texto
     * @return string com a informação de cliente
     */
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append("Código de cliente: ");
        s.append(getId());
        s.append(System.lineSeparator());
        return s.toString();
    }
    
     
    
}
