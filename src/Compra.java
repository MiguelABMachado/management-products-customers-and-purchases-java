import java.util.*;
import java.io.*;

/**
 *
 * @author Asus
 */
public class Compra implements Serializable {

    private String produto;
    private float preco;
    private int quantidade;
    private char tipo;
    private String cliente;
    private int mes;

    /**
     * Construtor de classe com os valores passados por argumento
     *
     * @param produto String com o código do produto
     * @param preco float com o preço
     * @param quantidade int com a quantidade
     * @param tipo char com o tipio de compra
     * @param cliente String com o código de cliente
     * @param mes int com o mês
     */
    public Compra(String produto, float preco, int quantidade, char tipo, String cliente, int mes) {
        this.produto = produto;
        this.preco = preco;
        this.quantidade = quantidade;
        this.tipo = tipo;
        this.cliente = cliente;
        this.mes = mes;
    }

    /**
     * Construtor de classe com os valores de um objecto dessa mesma classe
     *
     * @param c objecto com os valores
     */
    public Compra(Compra c) {
        cliente = c.getCliente();
        mes = c.getMes();
        preco = c.getPreco();
        produto = c.getProduto();
        quantidade = c.getQuantidade();
        tipo = c.getTipo();
    }

    /**
     * Devolve uma string com o código de cliente
     *
     * @return código de cliente
     */
    public String getCliente() {
        return cliente;
    }

    /**
     * Devolve um inteiro com o mês da compra
     *
     * @return valor do mês
     */
    public int getMes() {
        return mes;
    }

    /**
     * Devolve um float com o o preço do produto
     *
     * @return valor do preço
     */
    public float getPreco() {
        return preco;
    }

    /**
     * Devolve uma string com o código de produto
     *
     * @return código de produto
     */
    public String getProduto() {
        return produto;
    }

    /**
     * Devolve um inteiro com o valor da quantidade
     *
     * @return valor da quantidade
     */
    public int getQuantidade() {
        return quantidade;
    }

    /**
     * Devolve um char com o tipo de compra
     *
     * @return tipo de compra
     */
    public char getTipo() {
        return tipo;
    }

    /**
     * Altera o código do cliente para o passado por argumento
     *
     * @param cliente novo código de cliente
     */
    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    /**
     * Altera o mês para o passado por argumento
     *
     * @param mes novo mes da compra
     */
    public void setMes(int mes) {
        this.mes = mes;
    }

    /**
     * Altera o preço para o passado por argumento
     *
     * @param preco novo preço
     */
    public void setPreco(float preco) {
        this.preco = preco;
    }

    /**
     * Altera o código do produto para o passado por argumento
     *
     * @param produto novo código de cliente
     */
    public void setProduto(String produto) {
        this.produto = produto;
    }

    /**
     * Altera a quantidade para o passado por argumento
     *
     * @param quantidade nova quantidade
     */
    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    /**
     * Altera o código do cliente para o passado por argumento
     *
     * @param tipo novo tipo do cliente
     */
    public void setTipo(char tipo) {
        this.tipo = tipo;
    }

    /**
     * Cria um clone de um objecto desta classe
     *
     * @return o objecto clonado
     */
    public Compra clone() {
        return new Compra(this);
    }

    /**
     * Devolve a informação do objecto sobre a forma de texto
     *
     * @return string com a informação
     */
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append(getProduto());
        s.append(" ");
        s.append(getPreco());
        s.append(" ");
        s.append(getQuantidade());
        s.append(" ");
        s.append(getTipo());
        s.append(" ");
        s.append(getCliente());
        s.append(" ");
        s.append(getMes());
        return s.toString();
    }

    /**
     * Verifica se dois objectos são iguais
     *
     * @param o objecto a qual deve ser comparado
     * @return true se foram iguais, false caso contrário
     */
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if ((o == null) || (this.getClass() != o.getClass())) {
            return false;
        }
        Compra a = (Compra) o;

        return (this.produto.equals(a.getProduto()) && this.preco == a.getPreco() && this.quantidade == a.getQuantidade() && this.tipo == (a.getTipo()) && this.cliente.equals(a.getCliente()) && this.mes == a.getMes());
    }

}
