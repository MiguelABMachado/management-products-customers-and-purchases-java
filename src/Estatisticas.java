import java.util.*;
import java.io.*;

/**
 *
 * @author Asus
 */
public class Estatisticas implements Serializable {

    private String filename;
    private int tot_prod;
    private int tot_prod_comp;
    private int tot_prod_nao_comp;
    private int tot_clientes;
    private int tot_clientes_comp;
    private int tot_clientes_nao_comp;
    private int tot_compras_val_zero;
    private float tot_fat;
    private int[] tot_compras_mensal = new int[12];
    private float[] tot_faturacao_mensal = new float[12];
    private int[] tot_clientes_mensal = new int[12];
    ArrayList<Compra> invalidas;

    
    /**
     * Construtor de classe com os valores por defeito
     */
    public Estatisticas() {
        filename = "";
        tot_prod = 0;
        tot_prod_comp = 0;
        tot_prod_nao_comp = 0;
        tot_clientes = 0;
        tot_clientes_comp = 0;
        tot_clientes_nao_comp = 0;
        tot_compras_val_zero = 0;
        tot_fat = 0;
        init_array(tot_compras_mensal, 12);
        init_array(tot_faturacao_mensal, 12);
        init_array(tot_clientes_mensal, 12);
        invalidas = new ArrayList<>();
    }
    
    
     /**
     * Construtor de classe com os valores de um objecto dessa mesma classe
     *
     * @param e objecto com os valores
     */
    public Estatisticas(Estatisticas e) {
        filename = e.getFilename();
        tot_prod = e.getTot_prod();
        tot_prod_comp = e.getTot_prod_comp();
        tot_prod_nao_comp = e.getTot_prod_nao_comp();
        tot_clientes = e.getTot_clientes();
        tot_clientes_comp = e.getTot_clientes_comp();
        tot_clientes_nao_comp = e.getTot_clientes_comp();
        tot_compras_val_zero = e.getTot_compras_val_zero();
        tot_fat = e.getTot_fat();
        tot_compras_mensal = e.getTot_compras_mensal();
        tot_faturacao_mensal = e.getTot_faturacao_mensal();
        tot_clientes_mensal = e.getTot_clientes_mensal();
        invalidas = e.getInvalidas();
    }
    

    /**
     * Inicialização de um array de inteiros todo a 0
     * @param array  que vai ser inicializado
     * @param size tamanho do array
     */
    public void init_array(int[] array, int size) {
        int i;
        for (i = 0; i < size; i++) {
            array[i] = 0;
        }
    }

    /**
     * Inicialização de um array de floats todo a 0
     * @param array  que vai ser inicializado
     * @param size tamanho do array
     */
    public void init_array(float[] array, int size) {
        int i;
        for (i = 0; i < size; i++) {
            array[i] = 0;
        }
    }

    /**
     * Retorna o nome do ficheorp
     * @return nome do ficheiro
     */
    public String getFilename() {
        return filename;
    }

    /**
     * Retorna uma lista com todas as compras inválidas
     * @return lista com compras inválidas
     */
    public ArrayList<Compra> getInvalidas() {
        ArrayList<Compra> aux = new ArrayList<>();
        for (Compra c : invalidas) {
            aux.add(c.clone());
        }
        return aux;
    }

    /**
     * Retorna o total de clientes
     * @return o toal de clientes
     */
    public int getTot_clientes() {
        return tot_clientes;
    }

    /**
     * Retorna o total de clientes que realizaram compras
     * @return total de clientes que compraram
     */
    public int getTot_clientes_comp() {
        return tot_clientes_comp;
    }

    /**
     * Retorna o total de clientes que realizaram compras por mês
     * @return total de clientes que compraram por mês
     */
    public int[] getTot_clientes_mensal() {
        return tot_clientes_mensal.clone();
    }

    /**
     * Retorna o total de clientes que não realizaram compras por mês
     * @return total de clientes que não compraram por mês
     */
    public int getTot_clientes_nao_comp() {
        return tot_clientes_nao_comp;
    }

    /**
     * Retorna o total compras realizadas por mês
     * @return total de compras por mês
     */
    public int[] getTot_compras_mensal() {
        return tot_compras_mensal.clone();
    }
 
    /**
     * Retorna o total de compras de valor zero
     * @return total de compras de valor 0
     */
    public int getTot_compras_val_zero() {
        return tot_compras_val_zero;
    }

    /**
     * Retorna o total de facturação
     * @return total de facturação
     */
    public float getTot_fat() {
        return tot_fat;
    }

    /**
     * Retorna o total de facturação por mês
     * @return total facturado por mês
     */
    public float[] getTot_faturacao_mensal() {
        return tot_faturacao_mensal.clone();
    }

    /**
     * Retorna o total de produtos que realizaram compras por mês
     * @return tamanho de clientes que compraram por mÊs
     */
    public int getTot_prod() {
        return tot_prod;
    }

    /**
     * Retorna o total de produtos que foram comprados
     * @return total de produtos comprados
     */
    public int getTot_prod_comp() {
        return tot_prod_comp;
    }

    /**
     * Retorna o total de produtos que não foram comprados
     * @return total de produtos não comprados
     */
    public int getTot_prod_nao_comp() {
        return tot_prod_nao_comp;
    }

    /**
     * Altera o nome do ficheiro para o passado por argumento
     * @param filename  novo nome do ficheiro
     */
    public void setFilename(String filename) {
        this.filename = filename;
    }

    /**
     * Altera a lista de compras para a passada por argumento
     * @param i nova lista de compras
     */
    public void setInvalidas(ArrayList<Compra> i) {
        ArrayList<Compra> aux = new ArrayList<>();
        for (Compra c : i) {
            aux.add(c.clone());
        }
        invalidas = aux;
    }

    /**
     * Altera o valor do total de clientes para o passado por argumento
     * @param tot_clientes  novo valor do total de clientes 
     */
    public void setTot_clientes(int tot_clientes) {
        this.tot_clientes = tot_clientes;
    }

    /**
     * Altera o valor do total de clientes que fizeram compras para o passado por argumento
     * @param tot_clientes_comp   novo valor do total de clientes que fizeram compras 
     */
    public void setTot_clientes_comp(int tot_clientes_comp) {
        this.tot_clientes_comp = tot_clientes_comp;
    }

    /**
     * Altera o valor do total de clientes por mês  para o passado por argumento
     * @param tot_clientes_mensal   novo valor do total de clientes por mês
     */
    public void setTot_clientes_mensal(int[] tot_clientes_mensal) {
        this.tot_clientes_mensal = tot_clientes_mensal.clone();
    }

    /**
     * Altera o valor do total de clientes que nao realizaram compras por mês para o passado por argumento
     * @param tot_clientes_nao_comp   novo valor do total de clientes que nao realizaram compras por mês
     */
    public void setTot_clientes_nao_comp(int tot_clientes_nao_comp) {
        this.tot_clientes_nao_comp = tot_clientes_nao_comp;
    }

    /**
     * Altera o valor do total de compras por mês para o passado por argumento
     * @param tot_compras_mensal  novo valor do total compras por mês
     */
    public void setTot_compras_mensal(int[] tot_compras_mensal) {
        this.tot_compras_mensal = tot_compras_mensal.clone();
    }

    /**
     * Altera o valor do total de compras de valor zero para o passado por argumento
     * @param tot_compras_val_zero novo valor do total de compras de valor zero
     */
    public void setTot_compras_val_zero(int tot_compras_val_zero) {
        this.tot_compras_val_zero = tot_compras_val_zero;
    }

   /**
     * Altera o valor do total de facturação para o passado por argumento
     * @param tot_fat  novo valor do total de facturação
     */
    public void setTot_fat(float tot_fat) {
        this.tot_fat = tot_fat;
    }

    /**
     * Altera o valor do total de facturação por mês de valor zero para o passado por argumento
     * @param tot_faturacao_mensal  novo valor do total de facturação por mês
     */
    public void setTot_faturacao_mensal(float[] tot_faturacao_mensal) {
        this.tot_faturacao_mensal = tot_faturacao_mensal.clone();
    }

    /**
     * Altera o valor do total produtos para o passado por argumento
     * @param tot_prod novo valor do total de produtos
     */
    public void setTot_prod(int tot_prod) {
        this.tot_prod = tot_prod;
    }

    /**
     * Altera o valor do total produtos comprados para o passado por argumento
     * @param tot_prod_comp novo valor do total de produtos comprados
     */
    public void setTot_prod_comp(int tot_prod_comp) {
        this.tot_prod_comp = tot_prod_comp;
    }

    /**
     * Altera o valor do total produtos não comprados para o passado por argumento
     * @param tot_prod_nao_comp  novo valor do total de produtos não comprados
     */
    public void setTot_prod_nao_comp(int tot_prod_nao_comp) {
        this.tot_prod_nao_comp = tot_prod_nao_comp;
    }

    /**
     * Cria um clone de um objecto desta classe
     * @return o objecto clonado
     */
    public Estatisticas clone() {
        return new Estatisticas(this);
    }
    
      
    
    
     /**
     * Devolve a informação do objecto sobre a forma de texto
     *
     * @return string com a informação
     */
    public String toString() {
        int i;
        int[] aux;
        float[] aux2;
        StringBuilder s = new StringBuilder();
        s.append("Ficheiro lido: ");
        s.append(getFilename());
        s.append(System.lineSeparator());
        s.append("Total de produtos:");
        s.append(getTot_prod());
        s.append(System.lineSeparator());
        s.append("Total de produtos comprados:");
        s.append(getTot_prod_comp());
        s.append(System.lineSeparator());
        s.append("Total de produtos não comprados:");
        s.append(getTot_prod_nao_comp());
        s.append(System.lineSeparator());
        s.append("Total de clientes:");
        s.append(getTot_clientes());
        s.append(System.lineSeparator());
        s.append("Total de clientes que realizaram compras:");
        s.append(getTot_clientes_comp());
        s.append(System.lineSeparator());
        s.append("Total de clientes que não realizaram compras:");
        s.append(getTot_clientes_nao_comp());
        s.append(System.lineSeparator());
        s.append("Total de compras de valor zero:");
        s.append(getTot_compras_val_zero());
        s.append(System.lineSeparator());
        s.append("Total de faturação:");
        s.append(getTot_fat());
        return s.toString();
    }
    
    /**
     * Verifica se dois objectos são iguais
     *
     * @param o objecto a qual deve ser comparado
     * @return true se foram iguais, false caso contrário
     */
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if ((o == null) || (this.getClass() != o.getClass())) {
            return false;
        }
        Estatisticas a = (Estatisticas) o;

        return (this.tot_clientes == a.getTot_clientes()&& this.tot_prod == a.getTot_prod());
    }

}
