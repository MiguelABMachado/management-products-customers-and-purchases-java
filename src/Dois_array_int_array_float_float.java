import java.util.*;
import java.io.*;

/**
 *
 * @author Asus
 */
public class Dois_array_int_array_float_float implements Serializable {
    private int[] compras_mes_a_mes;
    private int[] tot_prod_mes_a_mes;
    private float[] faturacao_mes_a_mes;
    float tot_faturacao;

    /**
     * Construtor de classe com os valores por defeito
     */
    public Dois_array_int_array_float_float() {
        compras_mes_a_mes=new int[12];
        tot_prod_mes_a_mes=new int[12];
        faturacao_mes_a_mes=new float[12];
        tot_faturacao=0;
    }
    
    
    
    
    
    /**
     * Construtor de clasee com os valores passados por argumento
     * @param compras_mes_a_mes   array com o valor do total de compras por mês
     * @param tot_prod_mes_a_mes  array com o total de produtos por mês
     * @param faturacao_mes_a_mes  array com o valor do total de facturação mensal
     * @param tot_faturacao  float com o total facturado
     */
    public Dois_array_int_array_float_float(int[] compras_mes_a_mes, int[] tot_prod_mes_a_mes, float[] faturacao_mes_a_mes, float tot_faturacao) {
        this.compras_mes_a_mes = compras_mes_a_mes.clone();
        this.tot_prod_mes_a_mes = tot_prod_mes_a_mes.clone();
        this.faturacao_mes_a_mes = faturacao_mes_a_mes.clone();
        this.tot_faturacao = tot_faturacao;
    }
    
    
    /**
     * Construtor de classe com os valores de um objecto dessa mesma classe 
     * @param d objecto com os valores
     */
    public Dois_array_int_array_float_float(Dois_array_int_array_float_float d) {
        this.compras_mes_a_mes = d.getCompras_mes_a_mes();
        this.tot_prod_mes_a_mes = d.getTot_prod_mes_a_mes();
        this.faturacao_mes_a_mes = d.getFaturacao_mes_a_mes();
        this.tot_faturacao = d.getTot_faturacao();
    }
    

    /**
     * Devolve um array de inteiros com o valor do total de compras por mês
     * @return valor do total de compras por mês
     */
    public int[] getCompras_mes_a_mes() {
        return compras_mes_a_mes.clone();
    }

    /**
     * Devolve um array de floats com o valor do total facturado por mês
     * @return valor do total facturado por mês
     */
    public float[] getFaturacao_mes_a_mes() {
        return faturacao_mes_a_mes.clone();
    }

    /**
     * Devolve um float com o torak facturado
     * @return o total facturado
     */
    public float getTot_faturacao() {
        return tot_faturacao;
    }

    /**
     * Devolve um array de inteiros com o valor do total de produtos comprados por mês
     * @return valor do total de produtos comprados por mês
     */
    public int[] getTot_prod_mes_a_mes() {
        return tot_prod_mes_a_mes.clone();
    }

    /**
     * Altera o valor do array de total de compras para o passado por argumento
     * @param compras_mes_a_mes  novo valor do array de total de compras
     */
    public void setCompras_mes_a_mes(int[] compras_mes_a_mes) {
        this.compras_mes_a_mes = compras_mes_a_mes.clone();
    }

    /**
     * Altera o valor do array de faturação para o passado por argumento
     * @param faturacao_mes_a_mes  novo valor do array de faturação
     */
    public void setFaturacao_mes_a_mes(float[] faturacao_mes_a_mes) {
        this.faturacao_mes_a_mes = faturacao_mes_a_mes.clone();
    }

    
    /**
     * Altera o valor do total de faturação para o passado por argumento
     * @param tot_faturacao  novo valor do total de faturação
     */
    public void setTot_faturacao(float tot_faturacao) {
        this.tot_faturacao = tot_faturacao;
    }

    /**
     * Altera o valor do array de total de produtos para o passado por argumento
     * @param tot_prod_mes_a_mes  novo valor do array de total de produtos
     */
    public void setTot_prod_mes_a_mes(int[] tot_prod_mes_a_mes) {
        this.tot_prod_mes_a_mes = tot_prod_mes_a_mes.clone();
    }
    
    
    /**
     * Cria um clone de um objecto desta classe
     * @return o objecto clonado
     */
    public Dois_array_int_array_float_float clone(){
        return new Dois_array_int_array_float_float(this);
    }
    
    /**
     * Devolve a informação do objecto sobre a forma de texto
     * @return string com a informação
     */
    
   public String toString(){
        StringBuilder s = new StringBuilder();
        s.append(this.getTot_faturacao());
        s.append("      "); 
        for(int a:compras_mes_a_mes){
            s.append(a);
            s.append(System.lineSeparator());
        }
        for(int a:tot_prod_mes_a_mes){
            s.append(a);
            s.append(System.lineSeparator());
        }
        for(float a:faturacao_mes_a_mes){
            s.append(a);
            s.append(System.lineSeparator());
        }
        
        return s.toString();
    }
    
    /**
     * Verifica se dois objectos são iguais
     * @param o objecto a qual deve ser comparado
     * @return true se foram iguais, false caso contrário
     */
    public boolean equals(Object o) {
       if(o==this) return true;
        if((o==null)||(this.getClass()!=o.getClass())){
            return false;}
        Dois_array_int_array_float_float a=(Dois_array_int_array_float_float) o;
        return (this.compras_mes_a_mes.equals(a.getCompras_mes_a_mes()) && this.tot_prod_mes_a_mes.equals(a.getTot_prod_mes_a_mes())&& this.faturacao_mes_a_mes.equals(a.getFaturacao_mes_a_mes()));
    }
    
    
    
    
}
