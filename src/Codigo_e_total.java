import java.util.*;
import java.io.*;

/**
 *
 * @author Asus
 */
public class Codigo_e_total implements Serializable {
    
    String codigo;
    float total;
    
    /**
     * Construtor de classe com os valores por defeito
     */
    public Codigo_e_total() {
        codigo="";
        total=0;
    }
    
    /**
     * Construtor de clasee com os valores passados por argumento
     * @param codigo string com o valor do código
     * @param total float com o valor do total 
     */
    public Codigo_e_total(String codigo, float total) {
        this.codigo = codigo;
        this.total = total;
    }
    
    /**
     * Construtor de classe com os valores de um objecto dessa mesma classe 
     * @param c objecto com os valores
     */
    public Codigo_e_total(Codigo_e_total c){
        codigo=c.getCodigo();
        total=c.getTotal();
    }

    /**
     * Devolve uma string com o valor do código
     * @return valor do código
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Devolve um float com o valor de total
     * @return valor de total
     */
    public float getTotal() {
        return total;
    }

    /**
     * Altera o valor do código para o passado por argumento
     * @param codigo novo valor do código
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
    
    /**
     * Altera o valor de total para o passado por argumento
     * @param total valor de total
     */
    public void setTotal(float total) {
        this.total = total;
    }
    
    /**
     * Cria um clone de um objecto desta classe
     * @return o objecto clonado
     */
    public Codigo_e_total clone(){
        return new Codigo_e_total(this);
    }
    
    /**
     * Devolve a informação do objecto sobre a forma de texto
     * @return string com a informação de cliente
     */
    
    public String toString(){
        StringBuilder s = new StringBuilder();
        s.append(this.getCodigo());
        s.append("      ");
        s.append(this.getTotal());
        return s.toString();
    }
    
    /**
     * Verifica se dois objectos são iguais
     * @param o objecto a qual deve ser comparado
     * @return true se os Clientes foram iguais,false caso contrário
     */
    public boolean equals(Object o) {
       if(o==this) return true;
        if((o==null)||(this.getClass()!=o.getClass())){
            return false;}
        Codigo_e_total a=(Codigo_e_total) o;
        return (this.codigo.equals(a.getCodigo()));
    }
    
    
    
    
}
