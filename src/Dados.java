import java.util.*;
import java.io.*;

/**
 *
 * @author Asus
 */
public class Dados implements Serializable {
    
    private Cat_Clientes clientes; 
    private Cat_Produtos produtos;
    private Contabilidade contabilidade;
    private Compras compras;
    private Estatisticas stats;
    
    /**
     * Construtor de classe com os valores por defeito
     */
    public Dados(){
        clientes = new Cat_Clientes();
        produtos = new Cat_Produtos();
        contabilidade= new Contabilidade();
        compras=new Compras();
        stats=new Estatisticas();
    }
    
    /**
     * Construtor de classe com os valores de objecto dessa mesma classe
     * @param d objecto com os valores
     */
    public Dados(Dados d){
        clientes=d.getClientes();
        compras=d.getCompras();
        contabilidade=d.getContabilidade();
        produtos=d.getProdutos();
        stats=d.getStats();
    }

    /**
     * Devolve o catálogo de clientes
     * @return catálogo de clientes
     */
    public Cat_Clientes getClientes() {
        Cat_Clientes aux=clientes.clone();
        return aux;
    }

    /**
     * Devolve as compras organizadas por cliente
     * @return compras organizado por cliente
     */
    public Compras getCompras() {
        Compras aux=compras.clone();
        return aux;
    }

    /**
     * Devolve as compras organizadas por protudo
     * @return compras organizado por produto
     */
    public Contabilidade getContabilidade() {
        Contabilidade aux=contabilidade.clone();
        return aux;
    }
    
    /**
     * Devolve o catálogo de produtos
     * @return catálogo de produtos
     */
    public Cat_Produtos getProdutos() {
        Cat_Produtos aux=produtos.clone();
        return aux;
    }

    /**
     * Devolve as estatisticas das compras
     * @return estatisticas das compras
     */
    public Estatisticas getStats() {
        Estatisticas aux=stats.clone();
        return aux;
    }

    /**
     * Altera o valor do catálogo de clientes para o passado por argumento
     * @param clientes novo valor de clientes
     */
    public void setClientes(Cat_Clientes clientes) {
        this.clientes = clientes.clone();
    }

    /**
     * Altera o valor das compras organizadas por cliente para o passado por argumento
     * @param compras novo valor das compras organizadas por clientes
     */
    public void setCompras(Compras compras) {
        this.compras = compras.clone();
    }

    /**
     * Altera o valor das compras organizadas por produtos para o passado por argumento
     * @param contabilidade novo valor das compras organizadas por clientes
     */
    public void setContabilidade(Contabilidade contabilidade) {
        this.contabilidade = contabilidade.clone();
    }

    /**
     * Altera o valor do catálogo de produtos para o passado por argumento
     * @param produtos novo valor do catálogo de produtos
     */
    public void setProdutos(Cat_Produtos produtos) {
        this.produtos = produtos.clone();
    }

    /**
     * Altera o valor das estatisticas para o passado por argumento
     * @param stats novo valor das estatisticas
     */
    public void setStats(Estatisticas stats) {
        this.stats = stats.clone();
    }
    
    
    /**
     * Lê de um ficheiro de texto cujo nome e passado por argumento, e preenche o catálogo de clientes
     * @param s nome do ficheiro de texto
     */
    public void lerClientes2(String s){
        String line;
        Cliente aux;
        int tot=0;
        BufferedReader in = null;
            try {
                in = new BufferedReader(new FileReader(s));
            } catch (FileNotFoundException ex) {
                System.out.println("Ficheiro não existe");
            }
        try {
            while ((line = in.readLine())!= null) {
                aux= new Cliente(line);
                clientes.add_cliente(aux.clone());
                compras.add_compra(line);
            }
        } catch (IOException ex) {
            System.out.println("Ficheiro não existe");
        }
}
    
    
    /**
     * Lê de um ficheiro de texto cujo nome e passado por argumento, e preenche o catálogo de produtos
     * @param s nome do ficheiro de texto
     */
    public void lerProdutos2(String s){
        String line;
        Produto aux;
        BufferedReader in = null;
            try {
                in = new BufferedReader(new FileReader(s));
            } catch (FileNotFoundException ex) {
               System.out.println("Ficheiro não existe");
            }
        try {
            while ((line = in.readLine())!= null) {
                aux = new Produto(line);
                produtos.add_produto(aux.clone());
                contabilidade.add_compra(line);
            }
        } catch (IOException ex) {
                System.out.println("Ficheiro não existe");
        }
}
    
    
    /**
     * Lê de um ficheiro de texto cujo nome e passado por argumento, e preenche as compras e a contabilidade
     * @param s nome do ficheiro de texto
     */
    public void lerCompras2(String s){
        int valid=0,quantidade,mes,tot_val_zero=0;
        String line,produto,cliente;
        String[] partes;
        float preco,tot_fat=0;
        char tipo;
        Compra aux;
        HashSet<String> prod_comp=new HashSet<>();
        HashSet<String> client_comp=new HashSet<>();
        int[] tot_compras_mensal = new int[12];
        init_array(tot_compras_mensal,12);
        float[] tot_faturacao_mensal= new float[12];
        init_array(tot_faturacao_mensal,12);
        ArrayList<HashSet<String>> tot_clientes_mensal = new ArrayList<HashSet<String>>();
        init_array(tot_clientes_mensal,12);
        ArrayList<Compra> invalidas=new ArrayList<>();
        BufferedReader in = null;
            try {
                in = new BufferedReader(new FileReader(s));
            } catch (FileNotFoundException ex) {
                    System.out.println("Ficheiro não existe");
            }
        try {
            while ((line = in.readLine())!= null) {
                partes=line.split(" ");
                produto=partes[0];
                preco=Float.parseFloat(partes[1]);
                quantidade=Integer.parseInt(partes[2]);
                tipo=partes[3].charAt(0);
                cliente=partes[4];
                mes=Integer.parseInt(partes[5]);
                if(teste_validade(produto,cliente,mes,quantidade)){
                    valid++;
                    aux= new Compra(produto, preco, quantidade, tipo, cliente, mes);
                    contabilidade.add_compra(aux.clone());
                    compras.add_compra(aux.clone());
                    if(preco==0 || quantidade==0){
                        tot_val_zero++;
                    }
                }else{
                    aux= new Compra(produto, preco, quantidade, tipo, cliente, mes);
                    invalidas.add(aux);
                }
            }
        } catch (IOException ex) {
                System.out.println("Ficheiro não existe");  
        }
        //Atualizar as stats
        stats.setFilename(s);
        stats.setTot_compras_val_zero(tot_val_zero);
        stats.setInvalidas(invalidas);
        finish_stats();
}
    
    private boolean teste_validade(String produto,String cliente, int mes,int quantidade){
    if(mes<=0 || mes>12){
            return false;
    }
    if(quantidade<=0){
            return false;
    }
    if(!produtos.existe_produto(produto)){
            return false;
    }
    if(!clientes.existe_cliente(cliente)){
            return false;
    }
    return true;
}
    
    /**
     * Grava este objecto num ficheiro com o nome enviado por argumento
     * @param fich nome do ficheiro
     * @throws IOException não existe ficheiro com esse nome
     */
    public void gravaObj(String fich) throws IOException {
      ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fich));
      oos.writeObject(this);
      oos.flush(); oos.close();
    } 
  
  private void init_array(int[] array, int size){
        int i;
        for(i=0;i<size;i++){
            array[i]=0;
        }
    }
 
  private void init_array(float[] array, int size){
        int i;
        for(i=0;i<size;i++){
            array[i]=0;
        }
    }
  

   private void set_to_int(int[] tot_cliente_mensal, ArrayList<HashSet<String>> tot_clientes_mensal,int size) {
       int i; 
       for(i=0;i<12;i++){
           HashSet<String> aux = tot_clientes_mensal.get(i);
           tot_cliente_mensal[i]=aux.size();
       }
    }
   
   private void init_array(ArrayList<HashSet<String>> array, int size){
        int i;
        for(i=0;i<size;i++){
           HashSet<String> aux=new HashSet<>();
           array.add(i, aux);
        }
    }
   
   private void finish_stats(){
       //Produtos
       int tot_prod=produtos.get_size();
       stats.setTot_prod(tot_prod);
       int tot_prod_not_comp=contabilidade.get_tot_not_comp();
       stats.setTot_prod_nao_comp(tot_prod_not_comp);
       stats.setTot_prod_comp(tot_prod-tot_prod_not_comp);
       //Clientes
       int tot_clientes=clientes.get_size();
       stats.setTot_clientes(tot_clientes);
       int tot_client_not_comp=compras.get_tot_not_comp();
       stats.setTot_clientes_nao_comp(tot_client_not_comp);
       stats.setTot_clientes_comp(tot_clientes-tot_client_not_comp);
       //faturação
       float tot_fat=contabilidade.get_tot_fat();
       stats.setTot_fat(tot_fat);
       //dados mensais
       int[] tot_compras_mensal=contabilidade.get_tot_compras_por_mes(12);
       stats.setTot_compras_mensal(tot_compras_mensal);
       int i;
       float[] tot_faturacao_mensal = contabilidade.get_tot_faturacao_por_mes(12);
       stats.setTot_faturacao_mensal(tot_faturacao_mensal);
       int[] tot_cliente_mensal=compras.tot_dist_cliente_por_mes(12);
       stats.setTot_clientes_mensal(tot_cliente_mensal);
   }
   
   /**
    * Devolve as stats sobre forma de texto
    * @return string com a informação
    */
    public String stats_em_texto(){
       return stats.toString();
   }
    
    /**
     * Devolve uma lista com a informação das compras inválidas
     * @return lista com as compras inválidas 
     */
    public ArrayList<Compra> get_invalidas(){
        return stats.getInvalidas();
    }

    /**
     * Devolve um resumo da informação do cliente, o total de compras mês a mês,o total de produtos distintos mês a mês,o total de faturação mês a mês e o total de faturação mensal
     * @param cliente código de cliente
     * @return resumo da informação de cliente
     * @throws CodigoExisteException 
     */
    Dois_array_int_array_float_float get_dados_mensal_cliente(String cliente) throws CodigoExisteException {
        Dois_array_int_array_float_float aux=new Dois_array_int_array_float_float();
        try {
            aux=compras.get_dados_mensal_cliente(cliente);
        } catch (CodigoExisteException ex) {
            throw new CodigoExisteException();
        }
        return aux;
    }

    /**
     * Devolve uma lista com os códigos de produto nunca comprados por ordem alfabética e o seu total
     * @return lista com os códigos de produto
     */
    public ParArrayListInt get_by_order_prod_not_comp() {
        ParArrayListInt res=contabilidade.get_not_comp_by_order();
        return res;
    }

    /**
     * Devolve uma lista com os códigos de cliente nunca comprados por ordem alfabética e o seu total
     * @return lista com os códigos de clientes
     */
    public ParArrayListInt get_by_order_client_not_comp() {
        ParArrayListInt res=compras.get_not_comp_by_order();
        return res;
    }
    
    /**
     * Devolve o total de clientes e compras num mês
     * @param mes mês a procurar
     * @param i total de meses
     * @return total de clientes e compras do mês
     */
    public ParTotComprasTotClientes get_tot_cliente_compras_mes(int mes,int i){
        ParTotComprasTotClientes par = compras.get_tot_compras_clientes_num_mes(mes,i);
        return par;
    }
    
    /**
     * Devolve o total de vezes que um produto foi comprado e o total de faturação,ambos mês a mês e distinguidos entre N e P
     * @param produto código de produto
     * @return total de compras e faturação mês a mês com distinção
     */
    public DoisArraysInt_DoisArraysFloats prod_comp_fat_mes_a_mes(String produto){
        DoisArraysInt_DoisArraysFloats res=contabilidade.prod_fat_comp_mes_a_mes(produto, 12);
        return res;
    }
  
    /**
     * Devolve o total de faturação,o total de clientes e o total de compras de um determinado produto,mês a mês
     * @param produto código de produto
     * @param i total de meses
     * @return total de faturação, total de clientes e o total de compras, mês a mês
     */
    public TriTotComprasTotClientesTotFaturado get_tot_comp_cliente_mes_a_mes(String produto,int i){
        int[]totCompras=new int[i];
        int[] totClientes=new int[i];
        float[] totFatura=new float[i];
        totCompras=contabilidade.get_tot_compras_mes_a_mes(produto,i);
        totClientes=compras.get_tot_cliente_mes_a_mes(produto,i);
        totFatura=contabilidade.get_tot_faturacao_mes_a_mes(produto,i);
        TriTotComprasTotClientesTotFaturado tri = new TriTotComprasTotClientesTotFaturado(totCompras,totClientes,totFatura);
        return tri;
    }
    
    /**
     * Devolve todos os produtos que um cliente comprou por ordem decrescente de quantidade
     * @param cliente código do cliente
     * @return todos os produtos por ordem decrescente
     */
    public ArrayList<Codigo_e_total> get_all_comp_ord(String cliente){
        TreeSet<Codigo_e_total> ts=new TreeSet<>();
        ts=compras.get_all_comp_ord(cliente);
        ArrayList<Codigo_e_total> res=new ArrayList<>(ts);
        return res;
    }
    
    /**
     * Devolve todos os clientes ordenados por ordem decrescente de total de quantidade comprada do produto com o código enviado por argumento
     * @param produto código de produo
     * @return todos os cliente por ordem decrescente de quantidade comprada do produto
     */
    public TreeSet<Codigo_e_total> get_top_buyers_ord(String produto){
        TreeSet<Codigo_e_total> ts;
        ts=compras.get_top_buyers_ord(produto);
        return ts;
    }
    
    /**
     * Devolve os clientes que compraram maior variadade de produtos por ordem decrescente e o total de produtos que compraram
     * @return clientes que compraram maior variadade de produtos e total
     */
    public TreeSet<Codigo_e_total> get_most_diverse_ord(){
        TreeSet<Codigo_e_total> res;
        res=compras.get_most_diverse_ord();
        return res;
    }
    
    /**
     * Devolve todos os produtos ordenados por ordem decrescente de quantidade vendida
     * @return todos os produtos por ordem decrescente de quantidade
     */
    public TreeSet<Codigo_e_total> get_most_sold(){
        TreeSet<Codigo_e_total> res;
        res=contabilidade.get_most_sold();
        return res;
    }
    
    /**
     * Devolve o total de clientes que comprou o produto com o código enviado por argumento
     * @param codigo código do produto
     * @return total de clientes
     */
    public int tot_client_comp(String codigo) {
        return compras.get_tot_cliente_comp(codigo);
    }
    
    public int[] get_tot_compras_mensal(){
        int[] aux=new int[12];
        aux=stats.getTot_compras_mensal();
        return aux.clone();
    }
    
    public int[] get_tot_cliente_mensal(){
        int[] aux=new int[12];
        aux=stats.getTot_clientes_mensal();
        return aux.clone();
    }
    
    public float[] get_tot_faturacao_mensal(){
        float[] aux=new float[12];
        aux=stats.getTot_faturacao_mensal();
        return aux.clone();
    }
    
    /**
     * Devolve um clone deste objectoaux=stats.getTot_faturacao_mensal();
     * @return objecto clonado
     */
    public Dados clone(){
        return new Dados(this);
    }
    
    /**
     * Verifica se dois objectos são iguais
     * @param o objecto a qual deve ser comparado
     * @return true se foram iguais, false caso contrário
     */
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if ((o == null) || (this.getClass() != o.getClass())) {
            return false;
        }
        Dados a = (Dados) o;
        return (this.getClientes().equals(a.getClientes())&&this.getCompras().equals(a.getCompras())&&this.getContabilidade().equals(a.getContabilidade())&&this.getProdutos().equals(a.getProdutos())&&this.getStats().equals(a.getStats()));
    }
    
    /**
     * Devolve a informação do objecto sobre a forma de texto
     * @return string com a informação
     */
    public String toString(){
        StringBuilder s=new StringBuilder();
        s.append(clientes.toString());
        s.append(System.lineSeparator());
        s.append(produtos.toString());
        s.append(System.lineSeparator());
        s.append(compras.toString());
        s.append(System.lineSeparator());
        s.append(contabilidade.toString());
        s.append(System.lineSeparator());
        s.append(stats.toString());
        s.append(System.lineSeparator());
        return s.toString();
    }
    
}

