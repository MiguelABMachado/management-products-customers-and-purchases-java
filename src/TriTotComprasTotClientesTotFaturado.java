import java.util.*;
import java.io.*;

public class TriTotComprasTotClientesTotFaturado implements Serializable {
    
    private int[] tot_Compras;
    private int[] tot_Clientes;
    private float[] tot_Faturado;
    
    
    
     /**
     * Construtor de classe com os valores por defeito
     */
    public TriTotComprasTotClientesTotFaturado(){
        this.tot_Compras = new int[12];
        this.tot_Clientes = new int[12];
        this.tot_Faturado = new float[12];
    }
    
    
    /**
     * Construtor de clasee com os valores passados por argumento
     * @param tot_Compras  array com o valor do total de compras por mês
     * @param tot_Clientes int com o valor do total de clientes por mês
     * @param tot_Faturado float com o valor do total de facturação mensal
     */
    public TriTotComprasTotClientesTotFaturado(int[] tot_Compras,int[] tot_Clientes,float[] tot_Faturado){
        this.tot_Compras = tot_Compras;
        this.tot_Clientes = tot_Clientes;
        this.tot_Faturado = tot_Faturado;
    }

    /**
     * Construtor de classe com os valores de um objecto dessa mesma classe 
     * @param tri objecto com os valores
     */
    public TriTotComprasTotClientesTotFaturado(TriTotComprasTotClientesTotFaturado tri) {
        this.tot_Compras = tri.getTotCompras();
        this.tot_Clientes = tri.getTotClientes();
        this.tot_Faturado = tri.getTotFaturado();
    }

    
     /**
     * Devolve um array de inteiros com o valor do total de compras por mês
     * @return valor do total de compras por mês
     */
    public int[] getTotCompras(){
        return this.tot_Compras.clone();
    }
    
    
    /**
     * Devolve um array de inteiros com o valor do total de clientes por mês
     * @return valor do total de clientes por mês
     */
    public int[] getTotClientes() {
        return this.tot_Clientes.clone();
    }

    /**
     * Devolve um array de floats com o valor do total facturado por mês
     * @return valor do total facturado por mês
     */
    public float[] getTotFaturado(){
        return this.tot_Faturado.clone();
    }
    
    /**
     * Altera o valor do array de total de compras para o passado por argumento
     * @param tot_Compras novo valor do array de total de compras
     */
    public void setTotCompras(int[] tot_Compras) {
        this.tot_Compras = tot_Compras.clone();
    }

    /**
     * Altera o valor do total de clientes para o passado por argumento
     * @param tot_Clientes  novo valor do array total de clientes
     */
    public void setTotClientes(int[] tot_Clientes) {
        this.tot_Clientes = tot_Clientes.clone();
    }
    
    
    /**
     * Altera o valor do total facturado para o passado por argumento
     * @param tot_Faturado novo valor do total de facturação
     */
    public void setTotFaturado(float[] tot_Faturado) {
        this.tot_Faturado = tot_Faturado.clone();
    }
    
    /**
     * Cria um clone de um objecto desta classe
     * @return o objecto clonado
     */
    public TriTotComprasTotClientesTotFaturado clone(){
        return new TriTotComprasTotClientesTotFaturado(this);
    }
    
    /**
     * Devolve a informação do objecto sobre a forma de texto
     * @return string com a informação
     */
    
   public String toString(){
        StringBuilder s = new StringBuilder();
        for(int a:tot_Clientes){
            s.append(a);
            s.append(System.lineSeparator());
        }
        for(int a:tot_Compras){
            s.append(a);
            s.append(System.lineSeparator());
        }
        for(float a:tot_Faturado){
            s.append(a);
            s.append(System.lineSeparator());
        }
        
        return s.toString();
    }
    
    /**
     * Verifica se dois objectos são iguais
     * @param o objecto a qual deve ser comparado
     * @return true se foram iguais, false caso contrário
     */
    public boolean equals(Object o) {
       if(o==this) return true;
        if((o==null)||(this.getClass()!=o.getClass())){
            return false;}
        TriTotComprasTotClientesTotFaturado a=(TriTotComprasTotClientesTotFaturado) o;
        return (this.tot_Compras.equals(a.getTotCompras()) && this.tot_Clientes.equals(a.getTotClientes())&& this.tot_Faturado.equals(a.getTotFaturado()));
    }
    
    
    
    
    
    
}