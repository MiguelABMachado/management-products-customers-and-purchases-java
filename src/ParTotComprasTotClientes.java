import java.util.*;
import java.io.*;

public class ParTotComprasTotClientes implements Serializable {
    
    private int tot_Compras;
    private int tot_Clientes;
    
    
    /**
     * Construtor de classe com os valores por defeito
     */
    public ParTotComprasTotClientes(){
        this.tot_Compras = 0;
        this.tot_Clientes = 0;
    }
    
    /**
     * Construtor de clasee com os valores passados por argumento
     * @param tot_Compras  int com o valor do total de compras
     * @param tot_Clientes  int com o valor do total de clientes 
     */
    public ParTotComprasTotClientes(int tot_Compras,int tot_Clientes){
        this.tot_Compras = tot_Compras;
        this.tot_Clientes = tot_Clientes;
    }

    /**
     * Construtor de classe com os valores de um objecto dessa mesma classe 
     * @param par objecto com os valores
     */
    public ParTotComprasTotClientes(ParTotComprasTotClientes par) {
        this.tot_Compras = par.getTotCompras();
        this.tot_Clientes = par.getTotClientes();
    }

    
    /**
     * Devolve um inteiro com o valor do total de compras
     * @return valor do total de compras
     */
    public int getTotCompras(){
        return this.tot_Compras;
    }
    
    /**
     * Devolve um inteiro com o valor do total de clientes
     * @return valor do total de clientes
     */
    public int getTotClientes() {
        return this.tot_Clientes;
    }

    /**
     * Altera o valor do total de compras para o passado por argumento
     * @param tot_Compras novo valor do total de compras
     */
    public void setTotCompras(int tot_Compras) {
        this.tot_Compras = tot_Compras;
    }

    /**
     * Altera o valor do total de clientes para o passado por argumento
     * @param tot_Clientes novo valor do total de clientes
     */
    public void setTotClientes(int tot_Clientes) {
        this.tot_Clientes = tot_Clientes;
    }

    
    /**
     * Cria um clone de um objecto desta classe
     * @return o objecto clonado
     */
    public ParTotComprasTotClientes clone(){
        return new ParTotComprasTotClientes(this);
    }
    
    /**
     * Devolve a informação do objecto sobre a forma de texto
     * @return string com a informação
     */
    
    public String toString(){
        StringBuilder s = new StringBuilder();
        s.append(this.getTotCompras());
        s.append("      "); 
        s.append(this.getTotClientes());
        return s.toString();
    }
    
    /**
     * Verifica se dois objectos são iguais
     * @param o objecto a qual deve ser comparado
     * @return true se foram iguais, false caso contrário
     */
    public boolean equals(Object o) {
       if(o==this) return true;
        if((o==null)||(this.getClass()!=o.getClass())){
            return false;}
        ParTotComprasTotClientes a=(ParTotComprasTotClientes) o;
        return (this.tot_Clientes == a.getTotClientes() && this.tot_Compras == a.getTotCompras());
    }


}